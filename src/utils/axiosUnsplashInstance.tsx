"use client";
import axios from "axios";

const axiosUnsplashInstance = axios.create({
  headers:{
    Authorization:process.env.NEXT_PUBLIC_SECRET_KEY_UNSPLASH || ""
  },
  params: {
    client_id: process.env.NEXT_PUBLIC_ACCESS_KEY
  }
});

export default axiosUnsplashInstance;
