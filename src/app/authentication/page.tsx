"use client";
import { formLoginType } from '@/constant/type';
import { setAuthentication } from '@/redux/slices/authentication';
import AuthenticationService from '@/service/AuthenticationService';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/navigation'
import LogService from '@/service/LogService';
import Swal from 'sweetalert2';


const Authentication = () => {
    const router = useRouter()
    const dispatch = useDispatch()
    const [form, setForm] = useState({} as formLoginType)

    const handleForm = (event: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = event.target
        setForm(preState => ({ ...form, [name]: value }))
    }

    const login = () => {
        if (!form?.username) {
            Swal.fire({
                position: "center",
                icon: "error",
                title: "Please enter your username.",
                showConfirmButton: false,
                timer: 1500
            });
        } else if (!form?.password) {
            Swal.fire({
                position: "center",
                icon: "error",
                title: "Please enter your password.",
                showConfirmButton: false,
                timer: 1500
            });
        } else {
            AuthenticationService.login(form).then(({ data }) => {
                dispatch(setAuthentication(data))
                sessionStorage.setItem("accessToken", data?.token)
                sessionStorage.setItem("refreshToken", data?.refreshToken)
                insertLog(data?.username)
                router.push('/')
            }).catch((error) => {
                Swal.fire({
                    position: "center",
                    icon: "error",
                    title: error?.response?.data?.message,
                    showConfirmButton: false,
                    timer: 1500
                });
            })
        }
    }

    const insertLog = (username: string) => {
        LogService.insertLog({ username, loginTime: new Date() }).then(() => { })
    }

    return (
        <div className="h-lvh flex items-center justify-center  bg-stone-900 py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8 m-10">
                <div>
                    <h2 className="mt-6 text-center text-3xl font-extrabold text-white">Sign in to your account</h2>
                </div>
                <div className="rounded-md shadow-sm -space-y-px">
                    <div>
                        <label className="sr-only">Username</label>
                        <input name="username" value={form?.username} onChange={handleForm} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-orange-500 focus:border-orange-500 focus:z-10 sm:text-sm" placeholder="Username" />
                    </div>
                    <div>
                        <label className="sr-only">Password</label>
                        <input name="password" type="password" value={form?.password} onChange={handleForm} className="appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-orange-500 focus:border-orange-500 focus:z-10 sm:text-sm" placeholder="Password" />
                    </div>
                </div>

                <div className="flex items-center justify-between">
                    <div className="flex items-center">
                        <input id="remember-me" name="remember-me" type="checkbox" className="h-4 w-4 text-orange-500 focus:ring-orange-700 border-gray-300 rounded" />
                        <label htmlFor="remember-me" className="ml-2 block text-sm text-white">Remember me</label>
                    </div>

                    <div className="text-sm">
                        <a href="#" className="font-medium text-orange-500 hover:text-orange-700">
                            Forgot your password?
                        </a>
                    </div>
                </div>

                <div>
                    <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-orange-500 hover:bg-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-transparent" onClick={login}>
                        Sign in
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Authentication;
