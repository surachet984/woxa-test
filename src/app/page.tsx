"use client";
import Banner from "@/components/Banner";
import ImageList from "@/components/ImageList";
import Pagination from "@/components/Pagination";
import Skeleton from "@/components/Skeleton";
import { initialSearch } from "@/constant/initialState";
import { ImageInterface } from "@/constant/interface";
import UnsplashService from "@/service/UnsplashService";
import { useEffect, useState } from "react";

export default function Home() {
  const pageDefault = 1
  const [isLoading, setIsLoading] = useState(true);
  const [imageList, setImageList] = useState<ImageInterface[]>([]);
  const [currentPage, setCurrentPage] = useState(pageDefault);
  const [searchQuery, setSearchQuery] = useState('')
  const [orderBy, setOrderBy] = useState('latest')
  const itemsPerPage = 30;
  const totalItems = 999;
  const totalPages = Math.ceil(totalItems / itemsPerPage);


  useEffect(() => {
    if (searchQuery.length == 0) {
      fetchImage();
    }else{
      fetchImageSearch()
    }
  }, [orderBy, currentPage,searchQuery])

  const fetchImage = () => {
    setIsLoading(true);
    UnsplashService.findImage(currentPage, itemsPerPage, orderBy).then(({ data }) => {
      setImageList(data);
      setIsLoading(false);
    });
  };

  const fetchImageSearch = () => {
    setIsLoading(true);
    UnsplashService.searchImage(currentPage, itemsPerPage, searchQuery, orderBy).then(({ data }) => {
      setImageList(data?.results);
      setIsLoading(false);
    });
  }

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const handleSearchInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event?.target?.value)
  }

  const handleOderBy = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setOrderBy(event?.target?.value)
    setCurrentPage(pageDefault)
  }


  return (
    <div className="bg-[#FFF6E9] min-h-screen">
      <Banner value={searchQuery} valueOderBy={orderBy} onChangeSearch={handleSearchInputChange} onChangeOderBy={handleOderBy}  />
      <div className="flex items-center justify-center mt-2">
        <div className="w-full m-5">
          {isLoading ? (
            <Skeleton length={30} />
          ) : (
            <>
              <ImageList isData={imageList} />
              <Pagination
                currentPage={currentPage}
                totalPages={totalPages}
                onPageChange={handlePageChange}
              />
            </>
          )}
        </div>
      </div>
    </div>
  );
}
