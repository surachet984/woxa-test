import React, { useEffect } from 'react'
import { jwtDecode } from "jwt-decode";
import { setLogOut, setToken } from '@/redux/slices/authentication';
import { useDispatch } from 'react-redux';
import AuthenticationService from '@/service/AuthenticationService';

type Props = {
    children: JSX.Element | JSX.Element[];
};

export default function JWTGuard({ children }: Props) {
    const dispatch = useDispatch()
    const token = sessionStorage.getItem("accessToken")
    const refreshToken = sessionStorage.getItem("refreshToken")

    useEffect(() => {
        if (token && refreshToken) {
            const current: any = (Date.now() / 1000);
            const decoded: any = jwtDecode(token || "");
        
            if (current > decoded?.exp) {
                dispatch(setLogOut());
            } else {
                AuthenticationService.refreshToken({ refreshToken }).then(({ data }) => {
                    dispatch(setToken(data))
                    sessionStorage.setItem("accessToken",data?.token)
                    sessionStorage.setItem("refreshToken",data?.refreshToken)
                }).catch((error) => {
                    dispatch(setLogOut());
                })
            }
        }
    }, [])

    return (
        <div>{children}</div>
    )
}