import axios from "axios";

const ENDPOINT = {
    AUTH: {
        LOGIN: `${process.env.NEXT_PUBLIC_ENDPOINT_AUTH}/auth/login`,
        REFRESH_TOKRN: `${process.env.NEXT_PUBLIC_ENDPOINT_AUTH}/auth/refresh`
    },
};

const AuthenticationService = {
    login: (data: Object) => {
        return axios({
            method: "POST",
            url: ENDPOINT.AUTH.LOGIN,
            data
        })
    },
    refreshToken: (data: Object) => {
        return axios({
            method: "POST",
            url: ENDPOINT.AUTH.REFRESH_TOKRN,
            data
        })
    }
}

export default AuthenticationService;
