import axiosUnsplashInstance from "@/utils/axiosUnsplashInstance";

const ENDPOINT = {
    UNSPLASH: {
        FIND_IMAGE: (page: number, limit: number,orderBy:string) => `${process.env.NEXT_PUBLIC_ENDPOINT_UNSPLASH}photos?page=${page}&per_page=${limit}&order_by=${orderBy}`,
        SEARCH_IMAGE: (page: number, limit: number, query: string,orderBy:string) => `${process.env.NEXT_PUBLIC_ENDPOINT_UNSPLASH}search/photos?page=${page}&per_page=${limit}&query=${query}&order_by=${orderBy}`,
        FIND_IMAGE_BY_ID: (id: string) => `${process.env.NEXT_PUBLIC_ENDPOINT_UNSPLASH}photos/${id}`,
    },
};

const UnsplashService = {
    findImage: (page: number, limit: number,orderBy:string) => {
        return axiosUnsplashInstance({
            method: "GET",
            url: ENDPOINT.UNSPLASH.FIND_IMAGE(page, limit,orderBy)
        })
    },
    searchImage: (page: number, limit: number, query: string,orderBy:string) => {
        return axiosUnsplashInstance({
            method: "GET",
            url: ENDPOINT.UNSPLASH.SEARCH_IMAGE(page, limit, query,orderBy)
        })
    },
    findImageById:(id:string)=>{
        return axiosUnsplashInstance({
            method:"GET",
            url:ENDPOINT.UNSPLASH.FIND_IMAGE_BY_ID(id)
        })
    }
}

export default UnsplashService;
