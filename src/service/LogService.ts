import axios from "axios";

const ENDPOINT = {
    LOG: {
        INSERT_LOG: `${process.env.NEXT_PUBLIC_ENDPOINT_LOG}`,
    },
};

const LogService = {
    insertLog: (data: Object) => {
        return axios({
            method: "POST",
            url: ENDPOINT.LOG.INSERT_LOG,
            data
        })
    },
}

export default LogService;
