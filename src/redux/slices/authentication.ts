import { authenticationType } from "@/constant/type";
import { createSlice } from "@reduxjs/toolkit";

export const authenticationSlice = createSlice({
    name: "authentication",
    initialState: {
        user: {} as authenticationType,
        authentication: false,
    },
    reducers: {
        setAuthentication: (state: any, action: any) => {
            state.user = action.payload
            state.authentication = true
        },
        setLogOut: (state: any) => {
            state.user = {} as authenticationType
            state.authentication = false
        },
        setToken: (state: any, action: any) => {
            let { token, refreshToken } = action.payload
            state.user.token = token
            state.user.refreshToken = refreshToken
        }
    },
});

export const { setAuthentication, setLogOut,setToken } = authenticationSlice.actions;
export default authenticationSlice.reducer;
