import React from 'react';

interface PaginationProps {
    currentPage: number;
    totalPages: number;
    onPageChange: (page: number) => void;
}

const Pagination: React.FC<PaginationProps> = ({
    currentPage,
    totalPages,
    onPageChange,
}) => {
    const isFirstPage = currentPage === 1;
    const isLastPage = currentPage === totalPages;

    const handlePageChange = (page: number) => {
        onPageChange(page);
    };

    const renderPageNumbers = () => {
        const pageNumbers = [];
        const maxVisiblePages = 5;

        if (totalPages <= maxVisiblePages) {
            for (let i = 1; i <= totalPages; i++) {
                pageNumbers.push(
                    <button
                        key={i}
                        onClick={() => handlePageChange(i)}
                        className={`-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium ${currentPage === i
                                ? 'text-indigo-500 bg-indigo-50'
                                : 'text-gray-700 hover:bg-gray-50'
                            } ${currentPage === i ? 'cursor-default' : 'cursor-pointer'
                            }`}
                    >
                        {i}
                    </button>
                );
            }
        } else {
            let startPage = Math.max(1, currentPage - Math.floor(maxVisiblePages / 2));
            let endPage = Math.min(totalPages, startPage + maxVisiblePages - 1);

            if (endPage - startPage < maxVisiblePages - 1) {
                startPage = Math.max(1, endPage - maxVisiblePages + 1);
            }

            for (let i = startPage; i <= endPage; i++) {
                pageNumbers.push(
                    <button
                        key={i}
                        onClick={() => handlePageChange(i)}
                        className={`-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium ${currentPage === i
                                ? 'bg-slate-900 text-orange-600'
                                : 'text-gray-700 hover:bg-gray-50'
                            } ${currentPage === i ? 'cursor-default' : 'cursor-pointer'
                            }`}
                    >
                        {i}
                    </button>
                );
            }
        }

        return pageNumbers;
    };

    return (
        <div className="flex justify-center mt-4">
            <nav className="relative z-0 inline-flex shadow-sm">
                <button
                    onClick={() => handlePageChange(currentPage - 1)}
                    disabled={isFirstPage}
                    className={`relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 ${isFirstPage ? 'cursor-default' : 'cursor-pointer'
                        }`}
                >
                    Previous
                </button>
                {renderPageNumbers()}
                <button
                    onClick={() => handlePageChange(currentPage + 1)}
                    disabled={isLastPage}
                    className={`-ml-px relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50 ${isLastPage ? 'cursor-default' : 'cursor-pointer'
                        }`}
                >
                    Next
                </button>
            </nav>
        </div>
    );
};

export default Pagination;
