import { ImageInterface } from '@/constant/interface';
import React, { useState, useEffect } from 'react';
import { BiLike, BiDownload } from "react-icons/bi";
import { IoClose } from "react-icons/io5";

interface ModalProps {
    isOpen: boolean;
    onClose: () => void;
    isData: ImageInterface;
}

const ModalViewImage: React.FC<ModalProps> = ({ isOpen, onClose, isData }) => {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (isOpen) {
            setIsLoading(false);
        }
    }, [isOpen]);

    if (!isOpen) return null;

    return (
        <div className={`fixed inset-0 flex items-center justify-center transition-opacity duration-300 ${isOpen ? 'opacity-100' : 'opacity-0'} bg-black bg-opacity-50`} onClick={onClose}>
            <div className={`bg-white rounded-lg p-4 max-w-3xl w-full max-h-[90vh] overflow-y-auto transform transition-transform duration-300 ${isOpen ? 'translate-y-0 scale-100' : 'translate-y-4 scale-95'}`} onClick={e => e.stopPropagation()}>
                <div className="flex justify-between mb-5">
                    <div className="flex items-center">
                        {isLoading ? (
                            <div className="w-10 h-10 rounded-full bg-gray-300 animate-pulse" />
                        ) : (
                            <img className='object-cover h-10 rounded-full shadow-2xl' src={isData?.user?.profile_image?.large} alt="User profile" />
                        )}
                        <div className="text-sm ml-3">
                            {isLoading ? (
                                <div className="h-4 bg-gray-300 rounded w-24 animate-pulse" />
                            ) : (
                                <p className="leading-none font-bold">{`${isData?.user?.first_name} ${isData?.user?.last_name}`}</p>
                            )}
                        </div>
                    </div>
                    <button onClick={onClose} className="text-gray-500 hover:text-gray-800">
                       <IoClose size={30}/>
                    </button>
                </div>
                <div>
                    {isLoading ? (
                        <div className="w-full h-96 bg-gray-300 animate-pulse rounded-md" />
                    ) : (
                        <img
                            src={isData?.urls?.raw}
                            alt="Modal Content"
                            className="rounded-md w-full h-full object-cover"
                        />
                    )}

                    <div className="flex justify-between mt-2">
                        <div className='flex items-center'>
                            <BiLike className='mr-1'/>
                            <span className='font-bold'>{isData?.likes?.toLocaleString() || 0} people like this</span>
                        </div>
                        <div className='flex items-center'>
                            <BiDownload className='mr-1'/>
                            <h3 className='font-bold'>{isData?.downloads?.toLocaleString() || 0} Downloads</h3>
                        </div>
                    </div>
                    <div className="mt-5 pb-2">
                        {isData?.tags?.map((item, idx) => {
                            return (
                                <span className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2" key={idx}>{`#${item?.title}`}</span>
                            )
                        })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ModalViewImage;
