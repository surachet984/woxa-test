import React from 'react';

type Props = {
    length: number;
}

export default function Skeleton({ length }: Props) {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4 p-4 mx-auto">
            {Array.from({ length }).map((_, index) => (
                <div key={index} className='animate-pulse'>
                    <div className='bg-neutral-900 rounded-lg h-72'></div>
                </div>
            ))}
        </div>
    );
}
