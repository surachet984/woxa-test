import { ImageInterface } from '@/constant/interface';
import UnsplashService from '@/service/UnsplashService';
import React, { useState } from 'react';
import ModalViewImage from './ModalViewImage';

type Props = {
    isData: ImageInterface[];
};

export default function ImageList({ isData }: Props) {
    const [isOpen, setIsOpen] = useState(false)
    const [responseImage, setResponseImage] = useState({} as ImageInterface)

    const findImageById = (id: string) => {
        UnsplashService.findImageById(id).then(({ data }) => {
            setResponseImage(data)
            setIsOpen(!isOpen)
        })
    }

    return (
        <div className='grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-4 p-4 mx-auto px-4'>
            {isData?.map((item, idx) => (
                <div className='max-w-sm rounded overflow-hidden shadow-lg relative group cursor-zoom-in' key={idx} onClick={() => { findImageById(item?.id) }}>
                    <img
                        className="object-cover h-80 w-full transition duration-300 transform group-hover:scale-105"
                        loading="eager"
                        src={item?.urls?.small}
                        alt="Sunset in the mountains"
                    />
                    <div className="absolute top-0 right-0 m-3 bg-black bg-opacity-80 text-white p-1 rounded opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                        <p className="text-sm">{item?.likes?.toLocaleString() || 0} Likes</p>
                    </div>
                    <div className="absolute bottom-0 left-0 w-full p-3 bg-black bg-opacity-50 text-white opacity-0 group-hover:opacity-100 transition-opacity duration-300">
                        <div className="flex items-center">
                            <img className='object-cover h-10 rounded-full shadow-2xl' src={item?.user?.profile_image?.large} alt="User profile" />
                            <div className="text-sm ml-3">
                                <p className="leading-none">{item?.user?.first_name}</p>
                                <p className="">{item?.sponsorship?.tagline}</p>
                            </div>
                        </div>
                    </div>
                </div>
            ))}
            <ModalViewImage isOpen={isOpen} onClose={()=>{setIsOpen(!isOpen)}} isData={responseImage}/>
        </div>
    );
}
