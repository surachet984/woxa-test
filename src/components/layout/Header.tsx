import React, { useState } from 'react'
import { usePathname } from 'next/navigation'
import { useDispatch, useSelector } from 'react-redux'
import { IoMdLogOut } from "react-icons/io";
import { setLogOut } from '@/redux/slices/authentication';
import Swal from 'sweetalert2';
type Props = {}

export default function Header({ }: Props) {
  const pathname = usePathname()
  const dispatch = useDispatch()
  const { user, authentication } = useSelector((state: any) => state.authentication)

  const navigation = [
    { name: 'Product', href: '#' },
    { name: 'Features', href: '#' },
    { name: 'Marketplace', href: '#' },
    { name: 'Company', href: '#' },
  ]

  const logOut = () => {
    Swal.fire({
      title: "Do you want to log out?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: '#DC5F00',
      cancelButtonColor: "#373A40",
      confirmButtonText: "Log out"
    }).then((result) => {
      if (result.isConfirmed) {
        dispatch(setLogOut())
      }
    });
  }

  return (
    <div>
      <header className="absolute inset-x-0 top-0 z-50">
        <nav className="flex items-center justify-between p-6 lg:px-8" aria-label="Global">
          <div className="flex lg:flex-1">
            <a href="/" className="-m-1.5 p-1.5">
              <img
                src={"images/logo/logo.png"}
                alt="logo"
                width={150}
              />
            </a>
          </div>
          <div className="hidden lg:flex lg:gap-x-12 ">
            {navigation.map((item) => (
              <a key={item.name} href={item.href} className={`text-sm font-semibold leading-6 text-white`}>
                {item.name}
              </a>
            ))}
          </div>
          <div className="lg:flex lg:flex-1 lg:justify-end">
            {authentication ? (
              <div className="flex items-center">
                <img className='object-cover h-10 rounded-full shadow-2xl' src={user?.image} alt="User profile" />
                <div className="text-sm ml-3 text-white">
                  <p className="leading-none">{user?.username}</p>
                  <p className='hidden lg:flex'>{`${user?.firstName} ${user?.lastName}`}</p>
                </div>
                <div className="ml-5">
                  <IoMdLogOut className="text-white h-10 cursor-pointer" size={25} color="#f97316" onClick={logOut} />
                </div>
              </div>
            ) : (
              <a href="/authentication">
                <button type="submit" className="group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-full text-white bg-orange-500 hover:bg-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-orange-700">
                  Sign in
                </button>
              </a>
            )}
          </div>
        </nav>
      </header>
    </div>
  )
}