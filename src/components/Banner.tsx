import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { CiSearch } from "react-icons/ci";
import { ODER_BY_LIST } from '@/constant/oderByList';
type Props = {
    value: string
    valueOderBy:string
    onChangeSearch: (event: React.ChangeEvent<HTMLInputElement>) => void
    onChangeOderBy: (event: React.ChangeEvent<HTMLSelectElement>) => void
};

export default function Banner({ value,valueOderBy, onChangeSearch,onChangeOderBy }: Props) {
    const { user, authentication } = useSelector((state: any) => state.authentication)

    return (
        <div
            className="bg-cover bg-center h-96 flex items-center justify-center"
            style={{
                backgroundImage: `linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url('../images/banner/banner.webp')`,
                backgroundSize: 'cover',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
            }}
        >
            <div className="max-w-2xl w-full px-4 text-center">
                <h1 className='text-white text-3xl drop-shadow-lg mb-5'>
                    The best collection of free photos and videos shared by amazing creators.
                </h1>
                <div>
                    <div className="relative mt-2 rounded-md shadow-sm">
                        <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3 ">
                            <CiSearch />
                        </div>
                        <input
                            type="text"
                            name="price" 
                            className="block w-full  rounded-full border-0 text-center py-1.5 pl-7 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-orange-600 focus:outline-none focus:ring-2 focus:ring-transparent sm:text-sm sm:leading-6"
                            placeholder="Search for pictures"
                            value={value}
                            onChange={onChangeSearch}
                        />
                        <div className="absolute inset-y-0 right-0 flex items-center text-white rounded-lg">
                            <select
                                name="orderBy"
                                value={valueOderBy}
                                onChange={onChangeOderBy}
                                className="h-full rounded-full border-0 py-0 pl-2 pr-7 bg-orange-500 focus:outline-none focus:ring-2 focus:ring-transparent sm:text-sm"
                            >
                                {ODER_BY_LIST?.map((item,idx)=>{
                                    return(
                                        <option value={item?.value} key={idx}>{item?.title}</option>
                                    )
                                })}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
