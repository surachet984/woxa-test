export const ODER_BY_LIST = [
    {
        title: "Latest",
        value: "latest"
    },
    {
        title: "Oldest",
        value: "oldest"
    },
    {
        title: "Popular",
        value: "popular"
    },
    {
        title: "Views",
        value: "views"
    },
]