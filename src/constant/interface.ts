export interface ImageInterface {
    id:string;
    user: UserInterface
    likes: number
    alt_description: string;
    created_at: string;
    description:string;
    urls: UrlsInterface;
    sponsorship:SponsorshipInterface;
    tags:TagInterface[]
    views:number;
    downloads:number;
}

export interface UrlsInterface {
    full: string;
    raw: string;
    regular: string;
    small: string;
    thumb: string;
    small_s3: string;
}

export interface UserInterface {
    id: string;
    username: string;
    first_name: string;
    last_name: string;
    total_likes: number;
    total_photos: number;
    portfolio_url: string;
    profile_image: ProfileImageInterface
}

export interface ProfileImageInterface {
    large: string;
    medium: string;
    small: string;
}

export interface SponsorshipInterface{
    tagline:string
}

export interface TagInterface{
    title:string;
    type:string;
}

